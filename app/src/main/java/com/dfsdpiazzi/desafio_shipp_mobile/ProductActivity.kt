package com.dfsdpiazzi.desafio_shipp_mobile

import android.os.Bundle
import kotlinx.android.synthetic.main.finish_button.*
import org.jetbrains.anko.startActivity

class ProductActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)

        finish_button.setOnClickListener {
            startActivity<CostActivity>()
        }
    }
}
