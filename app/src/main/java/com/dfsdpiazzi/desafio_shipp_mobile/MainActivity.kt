package com.dfsdpiazzi.desafio_shipp_mobile

import android.os.Bundle
import com.google.android.libraries.places.api.Places
import android.view.View
import android.widget.AdapterView
import com.dfsdpiazzi.desafio_shipp_mobile.location.LocationProviderActivity
import com.dfsdpiazzi.desafio_shipp_mobile.location.OnLocationChangedListener
import com.dfsdpiazzi.desafio_shipp_mobile.location.PlaceAutocompleteAdapter
import kotlinx.android.synthetic.main.finish_button.*
import org.jetbrains.anko.startActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : LocationProviderActivity() {

    private val mOnItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Initialize the Places SDK.
        Places.initialize(applicationContext, getString(R.string.google_maps_key))

        /* TODO usar progress dialog somente se durante os testes estiver demorando para obter a localização.
            Provavelmente será instantâneo pois, neste caso, ele obtém a LAST KNOWN location, e não a CURRENT.
            Adicionar um indeterminate progress bar de forma não invasiva também pode ser interessante (DESIGN...)*/
        //val dialog = indeterminateProgressDialog(message = "Please wait a bit…", title = "Fetching data")
        //dialog.show()

        autocomplete.onItemSelectedListener = mOnItemSelectedListener
        onLocationChangedListener =
            OnLocationChangedListener {
                autocomplete.setAdapter(
                    PlaceAutocompleteAdapter(
                        this@MainActivity,
                        getRectangularBounds(it)
                    )
                )
            }

        finish_button.setOnClickListener {
            startActivity<ProductActivity>()
        }
    }

}
