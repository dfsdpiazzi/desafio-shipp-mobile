package com.dfsdpiazzi.desafio_shipp_mobile

import android.location.Location
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.RectangularBounds

// TODO setar bounds com raio (metros) e não diretamente em graus.
fun getRectangularBounds(location: Location): RectangularBounds {
    val lat = location.latitude
    val lng = location.longitude

    return RectangularBounds.newInstance(
        LatLng(lat - 1.0, lng - 1.0),
        LatLng(lat + 1.0, lng + 1.0)
    )
}