package com.dfsdpiazzi.desafio_shipp_mobile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_cost.*
import kotlinx.android.synthetic.main.finish_button.*
import org.jetbrains.anko.startActivity
import java.text.DecimalFormat

class CostActivity : BaseActivity() {

    private val mTextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            // Before any edit apply we must disable this Watcher
            // to avoid exhaustive loops or recursive behaviour.
            edit_text.removeTextChangedListener(this)

            // Text will be formatted automatically considering only
            // the numbers entered by the user. So we remove all unnecessary
            // characters before any casting or formatting call.
            val regex = Regex("[^0-9]")
            val str = regex.replace(s.toString(), "")

            // Formatting is only necessary if str is not empty.
            if (str.isEmpty() || Integer.parseInt(str) == 0) {
                // Show text hint.
                edit_text.setText("")
            } else {
                // Cast string to double and then appropriately format it.
                val f: Double = str.toDouble() / 100f
                val result: String = "R$ " + DecimalFormat("#,##0.00").format(f)

                // Set text content and reset cursor to end of string.
                edit_text.setText(result)
                edit_text.setSelection(result.length)
            }

            // After all this changes we may enable this Watcher.
            edit_text.addTextChangedListener(this)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cost)

        edit_text.addTextChangedListener(mTextWatcher)
        finish_button.setOnClickListener {
            startActivity<ProductActivity>()
        }
    }
}