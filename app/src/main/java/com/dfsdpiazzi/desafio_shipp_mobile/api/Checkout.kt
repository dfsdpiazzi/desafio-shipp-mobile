package com.dfsdpiazzi.desafio_shipp_mobile.api

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

class Checkout {
    class Request(
        val store_latitude: Double,
        val store_longitude: Double,
        val user_latitude: Double,
        val user_longitude: Double,
        val value: Double
    )

    class Response(
        val product_value: Double,
        val distance: Double,
        val total_value: Double,
        val fee_value: Double
    )

    internal interface Service {
        @POST("https://d9eqa4nu35.execute-api.sa-east-1.amazonaws.com/evaluate")
        fun checkout(@Body request: Request): Call<Response>
    }
}