package com.dfsdpiazzi.desafio_shipp_mobile.api

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

class Payment {
    class Request(
        val store_latitude: Double,
        val store_longitude: Double,
        val user_latitude: Double,
        val user_longitude: Double,
        val card_number: String,
        val cvv: String,
        val expiry_date: String,
        val value: Double
    )

    class Response(
        val message: String,
        val value: Double
    )

    internal interface Service {
        @POST("https://mdk3ljy26k.execute-api.sa-east-1.amazonaws.com/order")
        fun payment(@Body request: Request): Call<Response>
    }
}