package com.dfsdpiazzi.desafio_shipp_mobile.api

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.info
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

// TODO criar base class para Checkout e Payment, desde que torne o código mais funcional...
class API : AnkoLogger {
    private val retrofit = Retrofit.Builder()
        .baseUrl("http://localhost/") // We doesn't have a base URL and on Retrofit2 it's required. So I choose the localhost itself. It will be overflown on API calls.
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val checkoutService: Checkout.Service? =
        retrofit.create(Checkout.Service::class.java)

    private val paymentService: Payment.Service? =
        retrofit.create(Payment.Service::class.java)

    fun checkout(request: Checkout.Request) {
        val call = checkoutService!!.checkout(request)
        call.enqueue(object : Callback<Checkout.Response> {
            override fun onFailure(call: Call<Checkout.Response>, t: Throwable) {
                error("onFailure error")
            }

            override fun onResponse(
                call: Call<Checkout.Response>,
                response: Response<Checkout.Response>
            ) {
                info("response received: ${response.body()?.distance}")
            }
        })
    }

    fun payment(request: Payment.Request) {
        val call = paymentService!!.payment(request)
        call.enqueue(object : Callback<Payment.Response> {
            override fun onFailure(call: Call<Payment.Response>, t: Throwable) {
                error("onFailure error")
            }

            override fun onResponse(
                call: Call<Payment.Response>,
                response: Response<Payment.Response>
            ) {
                info("response received: ${response.body()?.message}")
            }

        })
    }
}