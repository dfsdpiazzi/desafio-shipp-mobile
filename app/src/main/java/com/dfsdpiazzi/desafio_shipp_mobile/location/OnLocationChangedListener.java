package com.dfsdpiazzi.desafio_shipp_mobile.location;

import android.location.Location;

// Interface was declared in Java because this way Kotlin
// lambdas expressions are generated automatically.
public interface OnLocationChangedListener {
    void onLocationChanged(Location location);
}
