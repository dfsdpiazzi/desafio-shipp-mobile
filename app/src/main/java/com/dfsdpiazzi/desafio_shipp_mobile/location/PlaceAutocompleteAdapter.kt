package com.dfsdpiazzi.desafio_shipp_mobile.location

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompletePrediction
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest

class PlaceAutocompleteAdapter(
    private val context: Context,
    private val locationRestriction: RectangularBounds
) :
    BaseAdapter(), Filterable {

    private val mPlaces = arrayListOf<AutocompletePrediction>()

    // Gets an instance of PlacesClient for a given Context.
    // Call initialize(Context, String) or initialize(Context, String, Locale) before calling this method.
    // More information at https://developers.google.com/places/android-sdk/reference/com/google/android/libraries/places/api/Places
    private val mPlacesClient = Places.createClient(context)

    // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
    // TODO and once again when the user makes a selection (for example when calling fetchPlace()).
    private val mToken = AutocompleteSessionToken.newInstance()

    // Inflates view for each item in ArrayList.
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val holder =
            if (convertView == null) {
                val view = LayoutInflater.from(context).inflate(
                    android.R.layout.simple_list_item_2,
                    parent, false
                )
                view.tag = ViewHolder(position, view)
                return view
            } else convertView.tag as ViewHolder
        holder.update(position)
        return convertView
    }

    // Improving Performance with the ViewHolder Pattern.
    private inner class ViewHolder(position: Int, convertView: View?) {
        private val text1: TextView = convertView!!.findViewById(android.R.id.text1)
        private val text2: TextView = convertView!!.findViewById(android.R.id.text2)

        init {
            update(position)
        }

        fun update(position: Int) {
            text1.text = mPlaces[position].getPrimaryText(null).toString()
            text2.text = mPlaces[position].getSecondaryText(null).toString()
            text2.isSingleLine = true
            text2.ellipsize = TextUtils.TruncateAt.END
        }
    }

    /**
     * Get item description by index.
     *
     * @param index position of the desired item in ArrayList.
     * @return object which AutocompleteTextView will extract description using toString() method.
     */
    override fun getItem(index: Int) = mPlaces[index].getFullText(null)

    /**
     * Get item unique id by index.
     *
     * @param index position of the desired item in ArrayList.
     * @return position of the desired item in ArrayList is unique.
     */
    override fun getItemId(index: Int): Long = index.toLong()

    /**
     * Number of Places currently available based on user query.
     *
     * @return size count of ArrayList.
     */
    override fun getCount(): Int = mPlaces.size

    override fun getFilter(): Filter {
        return object : Filter() {

            override fun publishResults(query: CharSequence?, results: FilterResults) =
                if (mPlaces.isEmpty())
                    notifyDataSetInvalidated()
                else notifyDataSetChanged()

            override fun performFiltering(query: CharSequence?): FilterResults {

                // Prevent NullPointerException and avoid useless calls to Places API.
                if (query.isNullOrEmpty()) {
                    mPlaces.clear()
                    return filterResults
                }

                // Use the builder to create a FindAutocompletePredictionsRequest.
                val request = FindAutocompletePredictionsRequest.builder()
                    .setLocationRestriction(locationRestriction)
                    .setCountry("br")
                    .setSessionToken(mToken)
                    .setQuery(query.toString())
                    .build()

                val task = mPlacesClient.findAutocompletePredictions(request)
                    .addOnSuccessListener { response ->
                        mPlaces.clear()
                        mPlaces.addAll(response.autocompletePredictions)
                    }
                    .addOnFailureListener { exception ->
                        if (exception is ApiException)
                            error("Place not found: " + exception.statusCode) // TODO send feedback to crashlytics...
                    }

                // Wait until task completes before return filter results.
                Tasks.await(task)
                return filterResults
            }

            /**
             * This builds a FilterResults object from ArrayList.
             */
            private val filterResults: FilterResults
                get() {
                    val results = FilterResults()
                    results.values = mPlaces
                    return results
                }
        }
    }
}